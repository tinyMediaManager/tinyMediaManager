package org.tinymediamanager.scraper.imdb.entities;

import org.tinymediamanager.scraper.entities.BaseJsonEntity;

public class ImdbPlaybackUrl extends BaseJsonEntity {
  public ImdbLocalizedString displayName = null;
  public String              mimeType    = "";
  public String              url         = "";
}
